module ElasticChat

open System
open Nest

open ChatTypes
open ChatUser
open Suave.Logging

type UserStoredMessage = {
    Id: string
    ChannelId: int
    GeneratedTime: DateTime
    OAuthId: string
    nick: string; 
    status: string; 
    email: string; 
    imageUrl: string
    Message: string
}

type ChanStoredConfig = {
    Id: string
    Name: string
    Users: List<string>
}  

type UserStoredConfig = {
    Id: string
    configHash: string
    OAuthId: string
    Nick: string; 
    Status: string; 
    Email: string; 
    ImageUrl: string
}

type UserConfig = {
    OAuthId: string
    nick: string; 
    status: string; 
    email: string; 
    imageUrl: string
}

let logger = Log.create "elasticPersistence"

// Configuration
let node = new Uri("http://127.0.0.1:9200")
let elConnSettings = new ConnectionSettings(node)
// mapping for user stored messages
elConnSettings.DefaultMappingFor<UserStoredMessage>(fun m -> m.IndexName("messages-chat") :> IClrTypeMapping<UserStoredMessage> ) |> ignore 
// mapping for user stored configuration
elConnSettings.DefaultMappingFor<UserStoredConfig>(fun m -> m.IndexName("usersconfig-chat") :> IClrTypeMapping<UserStoredConfig> ) |> ignore
// mapping for channel stored configuration
elConnSettings.DefaultMappingFor<ChanStoredConfig>(fun m -> m.IndexName("chansconfig-chat") :> IClrTypeMapping<ChanStoredConfig> ) |> ignore

let client = new ElasticClient(elConnSettings) //client instance

let computeMd5 (text: string) =
        use md5 = System.Security.Cryptography.MD5.Create()
        let hash = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(text))
        System.BitConverter.ToString(hash).Replace("-", "").ToLower()


let createIndices<'T when 'T : not struct> = 

    let createIdxResponse = client.CreateIndex(Indices.Index<'T>(), fun i -> 
        i.Mappings( fun ms -> 
            ms.Map<'T>( fun m -> 
                m.AutoMap() :> ITypeMapping
            ) :> IPromise<IMappings>
        ) :> ICreateIndexRequest
    )
    //TODO : get index name
    match createIdxResponse.Acknowledged with
    | true -> (logger.info (Message.eventX "Created {index} index into cluster" >> Message.setField "index" (Indices.Index<'T>().Name) ) )
    | false -> (logger.error (Message.eventX "Cannot create {index} index into cluster" >> Message.setField "index" (Indices.Index<'T>().Name) ) )

//TODO: Make functions Doc Exists general
let checkMsgDocExists (document: UserStoredMessage) = 
    let docPath =  new DocumentPath<UserStoredMessage>(document)
    let docResponse = client.DocumentExists<UserStoredMessage>(docPath, fun x -> 
        x.Index(Indices.Index<UserStoredMessage>()) |> ignore; 
        x.Type(TypeName.Create<UserStoredMessage>()) :> IDocumentExistsRequest )
    docResponse.Exists

let checkUsrDocExists (userDoc: UserStoredConfig) = 
    let docPath =  new DocumentPath<UserStoredConfig>(userDoc)
    let docResponse = client.DocumentExists<UserStoredConfig>(docPath, fun x -> 
        x.Index(Indices.Index<UserStoredConfig>()) |> ignore; 
        x.Type(TypeName.Create<UserStoredConfig>()) :> IDocumentExistsRequest )
    docResponse.Exists

let checkChanDocExists (userDoc: ChanStoredConfig) = 
    let docPath =  new DocumentPath<ChanStoredConfig>(userDoc)
    let docResponse = client.DocumentExists<ChanStoredConfig>(docPath, fun x -> 
        x.Index(Indices.Index<ChanStoredConfig>()) |> ignore; 
        x.Type(TypeName.Create<ChanStoredConfig>()) :> IDocumentExistsRequest )
    docResponse.Exists     

/// Function to initialize all indexes for the application.
let initializeIndices ()  = 
    let messagesIdxName = Indices.Parse("messages-chat")
    let usersIdxName = Indices.Parse("usersconfig-chat")
    let chansIdxName = Indices.Parse("chansconfig-chat")

    let messageIdxExistsResponse = client.IndexExists(new IndexExistsRequest(messagesIdxName)).Exists
    let usersIdxExistsResponse = client.IndexExists(new IndexExistsRequest(usersIdxName)).Exists
    let chansIdxExistsResponse = client.IndexExists(new IndexExistsRequest(chansIdxName)).Exists
    
    match messageIdxExistsResponse with
    | false -> (logger.info (Message.eventX "Index messages-chat doesn't exist"); createIndices<UserStoredMessage> )
    | true -> (logger.info (Message.eventX "Index messages-chat already exists"))

    match usersIdxExistsResponse with 
    | false -> (logger.info (Message.eventX "Index usersconfig-chat doesn't exist"); createIndices<UserStoredConfig> )
    | true -> (logger.info (Message.eventX "Index usersconfig-chat already exists"))

    match chansIdxExistsResponse with //PT3
    | false -> (logger.info (Message.eventX "Index chansconfig-chat doesn't exist"); createIndices<ChanStoredConfig> )
    | true -> (logger.info (Message.eventX "Index chansconfig-chat already exists"))

/// Function to map both AnonymousInfo and PersonInfo types attributes, to one general user type to store with chat message.
/// Return an option of user data/configuration type (UserConfigStore).
let mapUserConfig userRegistered  = 
    let tostr = Option.toObj
    match userRegistered with // check if exists RegisteredUser
    | Some (RegisteredUser (_, user)) -> 
        match user  with // check if RegisteredUser is a Person or Anonymous
        | Person u ->
            Some {OAuthId = Option.toObj u.oauthId;nick = u.nick; status = u.status; email = tostr u.email; imageUrl = Option.toObj u.imageUrl}
        | Anonymous u ->
            Some {OAuthId = "anon"; nick = u.nick; status = u.status; email = "anon"; imageUrl = Option.toObj u.imageUrl}
        | _ -> None 
    | None -> None

/// Function to update the user stored configuration after (/status,/avatar) user command 
let updateUserStoredConfig newUserInfo = 
    match newUserInfo with
    | Person  newPersonInfo -> 
        let configHashStr = computeMd5((Option.toObj newPersonInfo.oauthId)+newPersonInfo.nick+newPersonInfo.status+(Option.toObj newPersonInfo.email)+(Option.toObj newPersonInfo.imageUrl))
        let result = client.Search<UserStoredConfig>(fun (s:SearchDescriptor<UserStoredConfig>) ->
            new SearchRequest( 
                Query = new QueryContainer(query = BoolQuery(Must = [
                  new QueryContainer(query = new TermQuery(Field = new Field("configHash"), Value = configHashStr))
                ]))
            ) :> ISearchRequest)

        match result.Total > int64 0 with
        | true -> ()//logger.error (Message.eventX "DOC FOUND ")
        | false -> 
            let newUserDocument = {
                Id = computeMd5(newPersonInfo.nick)
                configHash = configHashStr
                OAuthId = Option.toObj newPersonInfo.oauthId
                Nick = newPersonInfo.nick
                Status = newPersonInfo.status
                Email = Option.toObj newPersonInfo.email
                ImageUrl = Option.toObj newPersonInfo.imageUrl
            }
            client.IndexDocument<UserStoredConfig>(newUserDocument) |> ignore
    | Anonymous newAnonInfo -> 
        let configHashStr = computeMd5("anon"+newAnonInfo.nick+newAnonInfo.status+"anon"+(Option.toObj newAnonInfo.imageUrl))
        let result = client.Search<UserStoredConfig>(fun (s:SearchDescriptor<UserStoredConfig>) ->
            new SearchRequest(
                Query = new QueryContainer(query = BoolQuery(Must = [
                  new QueryContainer(query = new TermQuery(Field = new Field("configHash"), Value = configHashStr))
                ]))
            ) :> ISearchRequest)

        match result.Total > int64 0 with
        | true -> ()//logger.error (Message.eventX "DOC FOUND ")
        | false -> 
            let newUserDocument = {
                Id = computeMd5(newAnonInfo.nick)
                configHash = configHashStr
                OAuthId = "anon"
                Nick = newAnonInfo.nick; 
                Status = newAnonInfo.status; 
                Email = "anon"; 
                ImageUrl = Option.toObj newAnonInfo.imageUrl
            }
            client.IndexDocument<UserStoredConfig>(newUserDocument) |> ignore
    | _ -> ()

/// Function to check if the userInfo provided is already stored. Store it if not.
/// Return the userInfo provided as parameter (if configuration not stored) or the stored configuration (if configuration was previously stored)
let checkUserStoredConfig userInfo = 
    match userInfo with
    | Person  personInfo -> 
        let configHashStr = computeMd5((Option.toObj personInfo.oauthId)+personInfo.nick+personInfo.status+(Option.toObj personInfo.email)+(Option.toObj personInfo.imageUrl))
        let userDocument = {
            Id = computeMd5(personInfo.nick)
            configHash = configHashStr
            OAuthId = Option.toObj personInfo.oauthId
            Nick = personInfo.nick; 
            Status = personInfo.status; 
            Email = Option.toObj personInfo.email; 
            ImageUrl = Option.toObj personInfo.imageUrl
        }
        match checkUsrDocExists userDocument with
        | true -> //check other attributes
            let prevDoc = client.Get<UserStoredConfig>(new GetRequest((Indices.Index<UserStoredConfig>()),(TypeName.Create<UserStoredConfig>()),(Id.From<UserStoredConfig>(userDocument))))
            let prevUser = Person {oauthId= Some prevDoc.Source.OAuthId; nick = prevDoc.Source.Nick; status = prevDoc.Source.Status; email= Some prevDoc.Source.Email; imageUrl = Some prevDoc.Source.ImageUrl}
            prevUser   
        | false -> 
            client.IndexDocument<UserStoredConfig>(userDocument) |> ignore;
            userInfo
    | Anonymous anonInfo -> 
        let configHashStr = computeMd5("anon"+anonInfo.nick+anonInfo.status+"anon"+(Option.toObj anonInfo.imageUrl))
        let userDocument = {
            Id = computeMd5(anonInfo.nick)
            configHash = configHashStr
            OAuthId = "anon"
            Nick = anonInfo.nick; 
            Status = anonInfo.status; 
            Email = "anon"; 
            ImageUrl = Option.toObj anonInfo.imageUrl
        }
        match checkUsrDocExists userDocument with
        | true -> 
            let prevDoc = client.Get<UserStoredConfig>(new GetRequest((Indices.Index<UserStoredConfig>()),(TypeName.Create<UserStoredConfig>()),(Id.From<UserStoredConfig>(userDocument))))
            let prevUser = Anonymous {nick = prevDoc.Source.Nick; status = prevDoc.Source.Status; imageUrl = Some prevDoc.Source.ImageUrl}
            prevUser           
        | false -> 
            client.IndexDocument<UserStoredConfig>(userDocument) |> ignore;
            userInfo
    | _ -> userInfo


/// Function to get the last/actual configuration of the message's author.
let getMsgUserConfig userNick =
    let result = client.Search<UserStoredConfig>(fun (s:SearchDescriptor<UserStoredConfig>) ->
      new SearchRequest(
        Size = (Nullable 1), //the only one config for that user
        Query = new QueryContainer(query = BoolQuery(Must = [
          new QueryContainer(query = new TermQuery(Field = new Field("id.keyword"), Value = computeMd5(userNick) ) )
        ]))
      ) :> ISearchRequest)

    result.Documents
        |> Seq.head

/// Function to update/store the channel configuration after user joins a channel .
let joinUpdateChanConfig channelName (ChannelId channelIdInt) userInfo =
    match channelIdInt>103 with
    | true ->
        match userInfo with
        | Person personInfo ->
            let chanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = channelName
                Users = List.singleton personInfo.nick
            }
            match checkChanDocExists chanDocument with
            | true -> 
                let prevDoc = client.Get<ChanStoredConfig>(new GetRequest((Indices.Index<ChanStoredConfig>()),(TypeName.Create<ChanStoredConfig>()),(Id.From<ChanStoredConfig>(chanDocument))))
                let containUser = prevDoc.Source.Users |> List.contains personInfo.nick
                match containUser with
                | true -> ()
                | false ->
                    let updateChanDocument = {
                        Id = sprintf "%i" channelIdInt
                        Name = channelName
                        Users = (prevDoc.Source.Users,chanDocument.Users) ||> List.append
                    }
                    client.IndexDocument<ChanStoredConfig>(updateChanDocument) |> ignore
            | false -> client.IndexDocument<ChanStoredConfig>(chanDocument) |> ignore
        | Anonymous anonInfo ->
            let chanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = channelName
                Users = List.singleton anonInfo.nick
            }
            match checkChanDocExists chanDocument with
            | true -> 
                let prevDoc = client.Get<ChanStoredConfig>(new GetRequest((Indices.Index<ChanStoredConfig>()),(TypeName.Create<ChanStoredConfig>()),(Id.From<ChanStoredConfig>(chanDocument))))
                let containUser = prevDoc.Source.Users |> List.contains anonInfo.nick
                match containUser with
                | true -> ()
                | false ->
                    let updateChanDocument = {
                        Id = sprintf "%i" channelIdInt
                        Name = channelName
                        Users = (prevDoc.Source.Users,chanDocument.Users) ||> List.append
                    }
                    client.IndexDocument<ChanStoredConfig>(updateChanDocument) |> ignore
            | false -> client.IndexDocument<ChanStoredConfig>(chanDocument) |> ignore
        | _ -> ()
    | false -> ()

/// Function to update/store the channel configuration after user left a channel .
let leaveUpdateChanConfig (ChannelId channelIdInt) userInfo =
    match channelIdInt>103 with
    | true ->
        match userInfo with
        | Person personInfo -> 
            let chanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = "none"
                Users = List.singleton personInfo.nick
            }
            let prevDoc = client.Get<ChanStoredConfig>(new GetRequest((Indices.Index<ChanStoredConfig>()),(TypeName.Create<ChanStoredConfig>()),(Id.From<ChanStoredConfig>(chanDocument))))
            let updateChanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = prevDoc.Source.Name
                Users = prevDoc.Source.Users |> List.filter(fun li -> not (System.String.Equals(li,personInfo.nick,System.StringComparison.Ordinal)) )
            }
            client.IndexDocument<ChanStoredConfig>(updateChanDocument) |> ignore
        | Anonymous anonInfo -> 
            let chanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = "none"
                Users = List.singleton anonInfo.nick
            }
            let prevDoc = client.Get<ChanStoredConfig>(new GetRequest((Indices.Index<ChanStoredConfig>()),(TypeName.Create<ChanStoredConfig>()),(Id.From<ChanStoredConfig>(chanDocument))))
            let updateChanDocument = {
                Id = sprintf "%i" channelIdInt
                Name = prevDoc.Source.Name
                Users = prevDoc.Source.Users |> List.filter(fun li -> not (System.String.Equals(li,anonInfo.nick,System.StringComparison.Ordinal)) )
            }
            client.IndexDocument<ChanStoredConfig>(updateChanDocument) |> ignore
        | _ -> ()
    | false -> ()

/// Function to retrieve all stored channels.
let getAllStoredChannels ()  =
    let result = client.Search<ChanStoredConfig>(fun (s:SearchDescriptor<ChanStoredConfig>) ->
            new SearchRequest( 
                Size = (Nullable 10000) //elasticsearch default constraint
            ) :> ISearchRequest)

    result.Documents 
        |> Seq.toList 
        |> List.map (fun doc -> (doc.Id,doc.Name))
        |> List.filter (fun (id,name) ->
            match System.Int32.TryParse id with //testing the API Reponse for results that does not contain id property as string of a int
            | (true, number) -> true
            | (false, _)     -> false
        ) 
        |> List.sortBy fst // sort for creation queue

/// Function to store the user message sent in a channel    
let storeUserMessage channelId messageUser (getUser: GetUser) = 
    async {
        match messageUser with
        | ChatMessage ((tsEvent,tsTime), (userId), (Message messageStr)) -> 
            let! userRegistered = getUser userId 
            match (mapUserConfig userRegistered) with
            | Some userConfig ->
                let messageDocumentId = computeMd5(userConfig.nick) + (tsTime.GetHashCode().ToString())
                let messageDocument = {
                    Id = messageDocumentId
                    ChannelId = channelId
                    GeneratedTime = tsTime
                    OAuthId = userConfig.OAuthId
                    nick = userConfig.nick 
                    status = userConfig.status 
                    email = userConfig.email 
                    imageUrl = userConfig.imageUrl
                    Message = messageStr
                }
                match checkMsgDocExists messageDocument with
                | true -> ()
                | false -> client.IndexDocument<UserStoredMessage>(messageDocument) |> ignore
            | None -> ()
        | _ -> ()
        
    }

/// Function to get all stored stored messages from a given channel
let getChanStoredMsg channelId  =
    let result = client.Search<UserStoredMessage>(fun (s:SearchDescriptor<UserStoredMessage>) ->
      new SearchRequest(
        Size = (Nullable 10000), //elasticsearch default constraint
        Query = new QueryContainer(query = BoolQuery(Must = [
          new QueryContainer(query = new TermQuery(Field = new Field("channelId"), Value = channelId))
        ]))
      ) :> ISearchRequest)
    
    result.Documents
        |> Seq.toList
        |> List.map (fun doc -> (doc.GeneratedTime,(doc.nick,doc.Message) ) ) 
        |> List.sortBy fst //sort by the oldest to the newest message 

